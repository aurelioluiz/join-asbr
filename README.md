# Venha trabalhar na ActualSales!

## A Empresa
A ActualSales é uma empresa de marketing digital focada em performance.

## Vagas

### Developer Full Stack
No momento estamos reestruturando o departamento de TI e precisamos de um programador experiente para **assumir a liderança da equipe de desenvolvimento web e desenvolver os projetos mais complexos**. Topa?

#### Requisitos:
- Experiência em Desenvolvimento Web, preferencialmente PHP
- Conseguir se virar em qualquer situação que surja (no escopo de TI)
- Sentir-se à vontade para ajudar e orientar os demais membros da sua equipe
- Facilidade para lidar com clientes
- Facilidade em trabalhar sobre pressão
- Querer vivenciar a gestão de uma equipe de TI
- Conhecimento e vontade (principalmente) de aplicar boas práticas e metodologias

#### Bônus:
- Interesse por infra e ops

#### Nosso stack atual:
- PHP (framework baseado no Zend)
- MySQL
- Git (Bitbucket com deploy contínuo)
- jQuery
- Bootstrap
- Apache
- Varnish
- **Importante ressaltar que esse stack está sempre aberto para novas tecnologias**

#### Projetos:
- Landing Pages
- Plataformas de eCommerce
- CRM
- Aplicativos Mobile e Facebook

#### Oferecemos:
- Ambiente informal
- Comissão Mensal sobre os resultados
- 15 Salários Anuais

Faixa salarial: 5k

#### Como me candidatar?
Não queremos ver o seu CV, mas sim o seu código.
Para isso, elaboramos um pequeno teste de aptidões que pode ser realizado no seu tempo.
As instruções para execução desse teste estão localizadas na pasta landing-page desse mesmo repositório.

#### Dúvidas
Para dúvidas ou mais informações, fique à vontade para nos contactar através do email <fernando.canteiro@actualsales.com.br>.


Obrigado e boa sorte!