var View = Backbone.View.extend({
	el: $('#form-container'),
    initialize: function() {
    	this.lead = new LeadModel();
    	this.$el.find('.form-control').val('');
    },
	events: {
        'click #btn-send' : 'send',
        'change select[name="regiao"]' : 'select',
    },
    send: function(){
    	this.lead.clear();
    	this.lead.set({
    		'nome': this.$el.find('input[name="nome"]').val(),
    		'data_nascimento': this.$el.find('input[name="data_nascimento"]').val(),
    		'email': this.$el.find('input[name="email"]').val(),
    		'telefone': this.$el.find('input[name="telefone"]').val(),
    		'regiao': this.$el.find('select[name="regiao"]').val(),
    		'unidade': this.$el.find('select[name="unidade"]').val()
        });
    	if(this.lead.isValid()) {
            $('#form-container').find('.alert').addClass('hide');
            $('#btn-send').prop('disabled', true).html('Enviando');
            this.lead.save(null, {
                success: function (model, response) {
                    if(response.success) {
                        $('#form-container').find('.form-step').eq(0).addClass('hide');
                        $('#form-container').find('.form-step').eq(1).removeClass('hide');
                        $('.panel-title').html('Obrigado pelo cadastro!');
                    } else {
                        $('#form-container').find('.alert').removeClass('hide').html(response.message);
                    }
                    $('#btn-send').prop('disabled', false).html('Enviar');
                }
            });
		} else {
    		$('#form-container').find('.alert').removeClass('hide').html(this.lead.validationError);
    	}
    },
    select: function(){
    	var options = ['Selecione a unidade mais próxima'];
    	var value = this.$el.find('select[name="regiao"]').val();
    	var unidade = this.$el.find('select[name="unidade"]');

    	if(value == 'Sul')
    		options = ['Porto Alegre', 'Curitiba'];
    	else if(value == 'Sudeste')
    		options = ['São Paulo', 'Rio de Janeiro', 'Belo Horizonte'];
    	else if(value == 'Centro-Oeste')
    		options = ['Brasília'];
    	else if(value == 'Nordeste')
    		options = ['Salvador', 'Recife'];
    	else if(value == 'Norte')
    		options = ['Não possui disponibilidade'];

    	unidade.html('');
    	$.each(options, function(i,e){
    		unidade.append('<option value="'+e+'">'+e+'</option>');
    	});
    }
});
var LeadModel = Backbone.Model.extend({
    url: 'app/leads.php',
	validate: function(attrs, options){
        
		if(attrs.nome == '')
			return 'O campo Nome é obrigatório.';
		else if(attrs.nome.length < 2)
			return 'O campo Nome deve conter ao menos 2 caracteres.';

		if(attrs.data_nascimento == '') {
            return 'O campo Data de Nascimento é obrigatório.';
        } else {
            var data = moment(attrs.data_nascimento, 'DD/MM/YYYY');
            if(!data.isValid())
                return 'O campo Data de Nascimento deve conter uma data válida.';
        }

        if(attrs.telefone == '')
			return 'O campo Telefone é obrigatório.';

        if(attrs.regiao == '')
            return 'O campo Região é obrigatório.';
	}
});

$(function () {

	$('input[name="data_nascimento"]').mask('99/99/9999');
	$('input[name="telefone"]').mask('(99) 99999999?9');

	var view = new View();
});