<?php

require 'config/init.php';
$post = json_decode(file_get_contents('php://input'));

if(!empty($post)) {

	// Formulário
	$nome = isset($post->nome) ? $post->nome : '';
	$data_nascimento = isset($post->data_nascimento) ? $post->data_nascimento : '';
	$email = isset($post->email) ? $post->email : '';
	$telefone = isset($post->telefone) ? $post->telefone : '';
	$regiao = isset($post->regiao) ? $post->regiao : '';
	$unidade = isset($post->unidade) ? $post->unidade : '';

	// Pontuação
	$pontuacao = 10;

	// Por Região
	if($regiao == 'Sul') {
		$pontuacao -= 2;
	} else if($regiao == 'Sudeste' && $unidade != 'São Paulo'){
		$pontuacao -= 1;
	} else if($regiao == 'Centro-Oeste') {
		$pontuacao -= 3;
	} else if($regiao == 'Nordeste') {
		$pontuacao -= 4;
	} else if($regiao == 'Norte') {
		$pontuacao -= 5;
		$unidade = 'INDISPONÍVEL';
	}

	// Por idade
	$dt = new DateTime('2016-06-01');
	$dt_n = DateTime::createFromFormat('d/m/Y', $data_nascimento);
	$idade = (int) $dt_n->diff($dt)->format('%r%y');

	if($idade < 18 || $idade >= 100) {
		$pontuacao -= 5;
	} else if($idade >= 40 && $idade <= 99) {
		$pontuacao -= 3;
	}

	// API 
	$response = $curl->post($endpoint, array(
		'nome' => $nome,
		'email' => $email,
		'telefone' => $telefone,
		'regiao' => $regiao,
		'unidade' => $unidade,
		'data_nascimento' => $dt_n->format('Y-m-d'),
		'score' => $pontuacao,
		'token' => $token
	));

	$api = array();

	if(!empty($response)) {
		$api = json_decode($response->body);
	
		// DB
		if($api->success) {
			$db->insert('leads', array(
				'nome' => $nome,
				'data_nascimento' => $dt_n->format('Y-m-d'),
				'email' => $email,
				'telefone' => $telefone,
				'regiao' => $regiao,
				'unidade' => $unidade,
				'pontuacao' => $pontuacao
			));
		}
	}

	echo json_encode($api);
}